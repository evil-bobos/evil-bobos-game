﻿namespace EvilBobos
{
    using System.Threading.Tasks;
    using Core;

    public static class MainApplication
    {
        public static bool Initialized { get; private set; }
        
        public static async Task Initialize()
        {
            if (Initialized)
                return;

            await ApplicationContext.Initialize();
            Initialized = true;
        }
    }
}