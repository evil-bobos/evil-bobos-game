﻿namespace EvilBobos.Utilities.Multiplayer
{
    using System.Threading.Tasks;
    using Unity.Netcode;
    using Unity.Netcode.Transports.UTP;
    using Unity.Networking.Transport.Relay;
    using Unity.Services.Relay;
    using UnityEngine;

    public static class RelayHelper
    {
        public static async Task<string> CreateRelay(int maxPlayers)
        {
            try
            {
                var allocation = await RelayService.Instance.CreateAllocationAsync(maxPlayers);
                var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

                Debug.Log($"[RelayController] Created relay with join code: {joinCode}");

                var relayData = new RelayServerData(allocation, "dtls");
                NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayData);
                NetworkManager.Singleton.StartHost();
                
                return joinCode;
            }
            catch (RelayServiceException e)
            {
                Debug.LogError($"[RelayController] Failed to create relay: {e.Message}");
                return null;
            }
        }
        
        public static async void JoinRelay(string joinCode)
        {
            try
            {
                var allocation = await RelayService.Instance.JoinAllocationAsync(joinCode);
                
                var relayData = new RelayServerData(allocation, "dtls");
                NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayData);
                NetworkManager.Singleton.StartClient();
            }
            catch (RelayServiceException e)
            {
                Debug.LogError($"[RelayController] Failed to join relay: {e.Message}");
            }
        }
    }
}