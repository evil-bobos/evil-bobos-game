﻿namespace EvilBobos.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] private GameObject prefab;
        [SerializeField] private int initialPoolSize = 10;
        [SerializeField] private bool canGrow = true;
        
        private List<GameObject> poolInstances;
        private List<GameObject> borrowedInstances;

        private void Awake()
        {
            poolInstances = new List<GameObject>();
            borrowedInstances = new List<GameObject>();
            
            for (var i = 0; i < initialPoolSize; i++)
            {
                CreateNewInstance();
            }
        }

        public GameObject Borrow()
        {
            var instance = poolInstances
                .Except(borrowedInstances)
                .FirstOrDefault(x => !x.gameObject.activeSelf);
            
            if (instance != null)
            {
                instance.gameObject.SetActive(true);
                borrowedInstances.Add(instance);
                return instance;
            }
            
            if (!canGrow)
                throw new Exception("Pool is empty");
                
            CreateNewInstance();
            
            var newInstance = poolInstances.Last();
            newInstance.gameObject.SetActive(true);
            borrowedInstances.Add(newInstance);
            
            return newInstance;
        }
        
        public void Release(GameObject instance)
        {
            instance.gameObject.SetActive(false);
            borrowedInstances.Remove(instance);
        }
        
        public void ReleaseAll()
        {
            foreach (var instance in borrowedInstances)
            {
                instance.gameObject.SetActive(false);
            }
            
            borrowedInstances.Clear();
        }

        private void CreateNewInstance()
        {
            var instance = Instantiate(prefab, transform);
            instance.SetActive(false);
            poolInstances.Add(instance);
        }
    }
}