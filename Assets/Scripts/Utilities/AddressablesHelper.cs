﻿namespace EvilBobos.Utilities
{
    using System;
    using System.Collections.Generic;
    using UnityEngine.AddressableAssets;
    using UnityEngine.ResourceManagement.AsyncOperations;

    public static class AddressablesHelper
    {
        private static readonly string[] _labelsCache = { "" };

        public static AsyncOperationHandle<IList<T>> LoadAssetsAsync<T>(string label, Action<T> callback = null)
        {
            _labelsCache[0] = label;
            return Addressables.LoadAssetsAsync(_labelsCache, callback, Addressables.MergeMode.Intersection);
        }
    }
}