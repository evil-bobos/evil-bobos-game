﻿namespace EvilBobos.Utilities
{
    using Unity.Services.Authentication;
    using Unity.Services.Lobbies.Models;

    public static class LobbyExtension
    {
        public static bool IsHost(this Lobby lobby)
        {
            return lobby.HostId == AuthenticationService.Instance.PlayerId;
        }
    }
}