﻿namespace EvilBobos.Constants
{
    using UnityEngine;

    public static class AnimationConstants
    {
        public static class Player
        {
            public static readonly int Jump = Animator.StringToHash("Jump");
            public static readonly int Move = Animator.StringToHash("Move");
            public static readonly int LeftHandUp = Animator.StringToHash("LeftHandUp");
            public static readonly int RightHandUp = Animator.StringToHash("RightHandUp");
            public static readonly int isKicking = Animator.StringToHash("isKicking");
            public static readonly int isLeftKick = Animator.StringToHash("isLeftKick");
            public static readonly int isMoving = Animator.StringToHash("isMoving");
        }
    }
}