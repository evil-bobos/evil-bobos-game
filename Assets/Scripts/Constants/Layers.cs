﻿namespace EvilBobos.Constants
{
    public static class Layers
    {
        public const int Level = 6;
        public const int Obstacles = 7;
        
        public const int LevelMask = 1 << Level;
        public const int ObstaclesMask = 1 << Obstacles;
    }
}