﻿namespace EvilBobos
{
    using System;
    using Core;
    using UnityEngine;

    public class Loader : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public async void InitializeGameplay(Action onDoneCallback = null)
        {
            if (!MainApplication.Initialized)
                await MainApplication.Initialize();
            
            onDoneCallback?.Invoke();
        }

        private void OnEnable()
        {
            ApplicationContext.PlayerInput.Enable();
        }

        private void OnDisable()
        {
            ApplicationContext.PlayerInput.Disable();
        }
    }
}