﻿namespace EvilBobos.Core.GameState
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;
    using Directory = System.IO.Directory;

    public static class GameStateFactory
    {
        private static string SAVEDIRECTORY = "/Saves";
        private static string SAVEEXTENSION = ".save";
        
        public static GameState CreateNewGameState(string saveName)
        {
            var formatter = new BinaryFormatter();
            var directoryPath = Application.persistentDataPath + SAVEDIRECTORY;
            
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            var savePath = $"{directoryPath}/{saveName}{SAVEEXTENSION}";
            var file = File.Create(savePath);
            var gameState = CreateNewGameState();
            
            formatter.Serialize(file, gameState);
            
            file.Close();

            return gameState;
        }
        
        public static GameState LoadGameState(string saveName)
        {
            var directoryPath = Application.persistentDataPath + SAVEDIRECTORY;
            var savePath = $"{directoryPath}/{saveName}{SAVEEXTENSION}";

            if (!File.Exists(savePath))
                return null;
            
            var formatter = new BinaryFormatter();
            var file = File.Open(savePath, FileMode.Open);

            try
            {
                var gameState = (GameState) formatter.Deserialize(file);
                file.Close();
                return gameState;
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to load game state: {e} at {savePath}");
                file.Close();
                return null;
            }
        }
        
        private static GameState CreateNewGameState()
        {
            return new GameState();
        }
        
    }
}