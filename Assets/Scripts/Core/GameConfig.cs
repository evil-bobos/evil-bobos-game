﻿namespace EvilBobos.Core
{
    using System;
    using UnityEngine;
    
    public enum Environment
    {
        Development,
        Production
    }
    
    [Serializable]
    public class MultiplayerSettings
    {
        [field: SerializeField] public Environment Environment { get; set; }
    }

    [CreateAssetMenu(menuName = "Data/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [SerializeField] private MultiplayerSettings multiplayer;

        public MultiplayerSettings Multiplayer => multiplayer;

        public void SetReleaseSettings()
        {
            multiplayer.Environment = Environment.Production;
        }

        public void SetDevelopmentSettings()
        {
            multiplayer.Environment = Environment.Development;
        } 
    }
}