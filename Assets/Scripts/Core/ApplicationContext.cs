﻿namespace EvilBobos.Core
{
    using System.Threading.Tasks;
    using Input;

    public static class ApplicationContext
    {
        public static IGameData GameData => gameData;
        public static PlayerInputActions PlayerInput { get; }

        public static bool IsInitialized { get; private set; }

        private static IGameData gameData;

        static ApplicationContext()
        {
            PlayerInput = new PlayerInputActions();
        }
        
        public static async Task Initialize()
        {
            gameData = new GameData();
            
            await LoadResources();
        }

        private static async Task LoadResources()
        {
            IsInitialized = true;
        }
    }
}