﻿#if UNITY_EDITOR
namespace EvilBobos.Editor
{
    using Core;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(GameConfig))]
    public class GameConfigEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            var gameConfig = (GameConfig) target;
            
            EditorGUILayout.Space(20);
            
            if (GUILayout.Button("Release Settings"))
                gameConfig.SetReleaseSettings();
            
            if (GUILayout.Button("Testing Preset #1"))
                gameConfig.SetDevelopmentSettings();
        }
    }
}
#endif