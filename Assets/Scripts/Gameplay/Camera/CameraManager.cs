﻿namespace EvilBobos.Gameplay.Camera
{
    using UnityEngine;

    public interface ICameraManager : ISubManager
    {
        Camera MainCamera { get; }
    }
    
    public class CameraManager : MonoBehaviour, ICameraManager
    {
        [SerializeField] private Camera mainCamera;
        
        public Camera MainCamera => mainCamera;
        
        private IGameContext gameContext;
        
        public void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
        }

        public void PostInitialize()
        {
        }

        public void UpdateManager()
        {
        }
    }
}