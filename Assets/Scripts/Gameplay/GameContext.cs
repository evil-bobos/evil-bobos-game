﻿namespace EvilBobos.Gameplay
{
    using Camera;
    using Core;
    using Network;
    using Player;
    using UI;
    using UnityEngine;
    using VisualEffects;

    public interface IGameContext
    {
        IGameData GameData { get; }
        
        UnityEngine.Camera GameCamera { get; }
        Canvas Canvas { get; }
        
        IGameplayManager GameplayManager { get; }
        IUIManager UIManager { get; }
        IPlayerManager PlayerManager { get; }
        ICameraManager CameraManager { get; }
        IVfxManager VfxManager { get; }
        ILocalNetworkManager LocalNetworkManager { get; }
    }
    
    public class GameContext : IGameContext
    {
        public IGameData GameData { get; set; }
        
        public UnityEngine.Camera GameCamera => CameraManager.MainCamera;
        public Canvas Canvas { get; set; }
        
        public IGameplayManager GameplayManager { get; set; }
        public IUIManager UIManager { get; set; }
        public IPlayerManager PlayerManager { get; set; }
        public ICameraManager CameraManager { get; set; }
        public IVfxManager VfxManager { get; set; }
        public ILocalNetworkManager LocalNetworkManager { get; set; }
    }
}