﻿namespace EvilBobos.Gameplay
{
    using System.Collections;
    using System.Collections.Generic;
    using Camera;
    using Constants;
    using Core;
    using Network;
    using Player;
    using UI;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using VisualEffects;

    public interface IGameplayManager
    {
        void ToggleCursor(bool toggle);
        bool IsInitialized { get; }
        void GameOver();
    }

    public interface ISubManager
    {
        void Initialize(IGameContext gameContext);
        void PostInitialize();
        void UpdateManager();
    }

    public class GameplayManager : MonoBehaviour, IGameplayManager
    {
        [SerializeField] private Canvas canvas;
        
        [Header("Managers")]
        [SerializeField] private UIManager uiManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private CameraManager cameraManager;
        [SerializeField] private VfxManager vfxManager;
        [SerializeField] private LocalNetworkManager localLocalNetworkManager;

        public bool IsInitialized { get; private set; }

        private IGameContext gameContext;

        private List<ISubManager> subManagers;

        public void ToggleCursor(bool toggle)
        {
            Cursor.lockState = toggle ? CursorLockMode.None : CursorLockMode.Locked;
            Cursor.visible = toggle;
        }

        private async void Start()
        {
            // yield return new WaitUntil(() => ApplicationContext.IsInitialized);

            await ApplicationContext.Initialize();
            
            gameContext = new GameContext
            {
                GameData = ApplicationContext.GameData,
                
                Canvas = canvas,
                
                GameplayManager = this,
                UIManager = uiManager,
                PlayerManager = playerManager,
                CameraManager = cameraManager,
                VfxManager = vfxManager,
                LocalNetworkManager = localLocalNetworkManager
            };
            
            subManagers = new List<ISubManager>
            {
                uiManager,
                playerManager,
                cameraManager,
                vfxManager,
                localLocalNetworkManager
            };
            
            foreach (var subManager in subManagers)
            {
                subManager.Initialize(gameContext);
            }
            
            foreach (var subManager in subManagers)
            {
                subManager.PostInitialize();
            }
            
            ToggleCursor(false);
            
            IsInitialized = true;
        }

        private void Update()
        {
            if (!IsInitialized)
                return;
            
            foreach (var subManager in subManagers)
            {
                subManager.UpdateManager();
            }
        }

        public void GameOver()
        {
            SceneManager.LoadSceneAsync((int)Scenes.MainMenu);
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if(hasFocus)
                ToggleCursor(false);
        }
    }
}