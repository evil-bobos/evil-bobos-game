﻿namespace EvilBobos.Gameplay.VisualEffects
{
    using UnityEngine;

    public interface IVfxManager : ISubManager
    {
        
    }
    
    public class VfxManager : MonoBehaviour, IVfxManager
    {
        private IGameContext gameContext;
        
        public void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
        }

        public void PostInitialize()
        {
        }

        public void UpdateManager()
        {
        }
    }
}