using System.Collections;
using System.Collections.Generic;
using EvilBobos.Gameplay.Player;
using UnityEngine;

namespace EvilBobos
{
    using Gameplay.Network;
    using Unity.Netcode;

    public class PickPowerUp : NetworkBehaviour
    {
        [Header("Icon")]
        [SerializeField] private float iconHeight = 4.5f;
        [SerializeField] private float rotationSpeed = 50f;
        [SerializeField] private float verticalSpeed = 2f;
        [SerializeField] private float verticalAmplitude = 0.5f;

        [Header("Boost")]
        [SerializeField] private PlayerController playerController;
        [SerializeField] private PowerUp.PowerUpType allowedPowerUp;
        private PowerUp powerUp;
        private Transform currentRef;
        private Transform item;
        private float originalY;

        private void OnTriggerEnter(Collider other)
        {
            if (
                !other.GetComponent<PowerUp>()
                || !other.GetComponent<PowerUp>().spawned
                || other.GetComponent<PowerUp>().GetPowerUpType() != allowedPowerUp
            ) return;

            powerUp = other.GetComponent<PowerUp>();
            powerUp.PickItem();
            powerUp.playerController = playerController;
            currentRef = powerUp.RefItem;

            if (item != null) Destroy(item.gameObject);
            item = Instantiate(currentRef, transform.position, Quaternion.identity).transform;
            originalY = playerController.transform.position.y + iconHeight;
            LocalNetworkManager.Instance.SpawnNetworkPowerUp(item);
        }

        private void Update()
        {
            if (playerController != null && item != null && IsOwner) MovePowerUpServerRpc(item.position, transform.position);
            if (powerUp != null && Input.GetKey(playerController.activationKey))
            {
                powerUp.UsePowerUp();
                DestroyPowerUp();
            }
        }

        public void DestroyPowerUp()
        {
            powerUp = null;
            currentRef = null;
            LocalNetworkManager.Instance.DestroyNetworkPowerUp(item);
        }

        [ServerRpc]
        private void MovePowerUpServerRpc(Vector3 pos, Vector3 playerPos)
        {
            originalY = playerPos.y + iconHeight;
            item.transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
            float verticalMovement = Mathf.Sin(Time.time * verticalSpeed) * verticalAmplitude;
            item.transform.position = new Vector3(playerPos.x, originalY + verticalMovement, playerPos.z);
        }
    }
}
