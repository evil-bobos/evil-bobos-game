using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EvilBobos
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private float maxHealth = 100f;
        [SerializeField] private Slider healthBar;
        [HideInInspector] public float currentHealth;

        private void Start()
        {
            currentHealth = maxHealth;
            UpdateHealthBar();
        }

        public void TakeDamage(float damageAmount)
        {
            currentHealth -= damageAmount;
            currentHealth = Mathf.Max(currentHealth, 0f);
            UpdateHealthBar();
        }

        private void UpdateHealthBar()
        {
            healthBar.value = currentHealth / maxHealth;
        }

    }
}
