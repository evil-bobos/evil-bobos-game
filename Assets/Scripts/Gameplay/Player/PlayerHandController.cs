﻿namespace EvilBobos.Gameplay.Player
{
    using System.Collections.Generic;
    using System.Linq;
    using Interaction;
    using UnityEngine;

    public class PlayerHandController : MonoBehaviour
    {
        [SerializeField] private KeyCode grabKey;
        [SerializeField] private LayerMask grabbableLayerMask;
        [SerializeField] private Rigidbody hand;
        [SerializeField] private float handDmg = 10f;
        [SerializeField] private PlayerController controller;

        private IGrabbable heldObject;
        private List<IGrabbable> availableObjects;
        
        public void Initialize()
        {
            availableObjects = new List<IGrabbable>();
        }
        
        public void UpdateController()
        {
            if (Input.GetKeyDown(grabKey))
            {
                GrabItem();
            }
            
            if (Input.GetKeyUp(grabKey))
            {
                ReleaseItem();
            }

            LookForHit();
        }
        
        private void LookForHit() {
            var closestGrabbable = GetClosestGrabbable();
            if (closestGrabbable == null) return;

            var hitable = closestGrabbable.Transform.GetComponent<Health>();
            if (hitable == null) return;

            if (controller.canHitEnemy == false) return;
            hitable.TakeDamage(handDmg);
            controller.canHitEnemy = false;
        }

        private void GrabItem()
        {
            if (heldObject != null)
                return;
            
            var closestGrabbable = GetClosestGrabbable();
                
            if (closestGrabbable == null) 
                return;
            
            Debug.Log($"[PlayerHandsController] Grabbing {closestGrabbable}");
            
            closestGrabbable.Grab(hand);
                
            heldObject = closestGrabbable;
        }
        
        private void ReleaseItem()
        {
            if (heldObject == null)
                return;
            
            Debug.Log($"[PlayerHandsController] Releasing {heldObject}");
            
            heldObject.Release();
            heldObject = null;
        }

        private IGrabbable GetClosestGrabbable()
        {
            if (availableObjects.Count == 0)
                return null;
            
            return availableObjects
                .OrderBy(x => Vector3.Distance(transform.position, x.Transform.position))
                .First();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<IGrabbable>(out var grabbable) && !availableObjects.Contains(grabbable))
            {
                availableObjects.Add(grabbable);
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent<IGrabbable>(out var grabbable) && availableObjects.Contains(grabbable))
            {
                availableObjects.Remove(grabbable);
            }
        }
    }
}