namespace EvilBobos.Gameplay.Player
{
    using UnityEngine;

    public class CopyMotion : MonoBehaviour
    {
        [SerializeField] private bool inverse;
        
        public Transform TargetLimb { get; set; }

        private Quaternion startRot;
        private ConfigurableJoint myJoint;

        private void Start() {
            myJoint = GetComponent<ConfigurableJoint>();
            startRot = transform.localRotation;
        }

        private void Update() {
            if (!inverse) myJoint.targetRotation = TargetLimb.localRotation * startRot;
            else myJoint.targetRotation = Quaternion.Inverse(TargetLimb.localRotation) * startRot;
        }
    }
}
