using UnityEngine;

namespace EvilBobos.Gameplay.Player {
    public class UpdateGrounding : MonoBehaviour 
    {
        [SerializeField] private PlayerController player; 

        private void OnCollisionEnter(Collision collision) {
            player.IsGrounded = true;
        }
    }
}
