﻿namespace EvilBobos.Gameplay.Player
{
    using Unity.Netcode;
    using Cinemachine;
    using UnityEngine;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Constants;

    public class PlayerController : NetworkBehaviour
    {
        [Header("Movement")]
        [SerializeField] private float movementSpeed = 500;
        [SerializeField] private float rotationSpeed = 5f;
        [SerializeField] private float jumpForce = 10000;
        [SerializeField] private float atackCooldown = 2f;
        [SerializeField] public KeyCode activationKey = KeyCode.X;

        [Header("References")]
        [SerializeField] private Transform skelleton;
        [SerializeField] private CinemachineVirtualCameraBase virtualCamera;
        [SerializeField] private Camera playerCamera;
        [SerializeField] private AudioListener audioListener;
        [SerializeField] private Rigidbody hips;
        [SerializeField] private List<PlayerHandController> handControllers;
        [SerializeField] private float hitAnimationTime = 2f;
        public Health health;

        [HideInInspector] public bool canHitEnemy = false;
        public bool IsGrounded { get; set; } = true;
        public float MovementSpeed { get => movementSpeed; set => movementSpeed = value; }
        public float JumpForce { get => jumpForce; set => jumpForce = value; }
        public Transform Transform => hips.transform;

        private Animator animator;

        private bool isLeftKick = false;
        private bool atackCooldownOn = false;

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            audioListener.enabled = IsOwner;

            virtualCamera.transform.SetParent(null);
            playerCamera.transform.SetParent(null);

            if (!IsOwner)
            {
                virtualCamera.Priority = 0;
            }
        }

        public void Initialize(Animator animator, Transform dollTransform)
        {
            this.animator = animator;

            foreach (var handController in handControllers)
            {
                handController.Initialize();
            }

            InitializeMotion(dollTransform);
        }

        private void Update()
        {
            if (!IsOwner)
                return;

            foreach (var handController in handControllers)
            {
                handController.UpdateController();
            }

            HandleMovment();

            if (Input.GetKey(KeyCode.Space))
                HandleJumpServerRpc();

            HandleKick();
            HandlePunch();
        }

        [ServerRpc(RequireOwnership = false)]
        private void HandleJumpServerRpc()
        {
            if (IsGrounded)
            {
                hips.AddForce(new Vector3(0, jumpForce, 0));
                IsGrounded = false;
            }
        }

        private void HandleMovment()
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            var camTransform = playerCamera.transform;
            var direction = camTransform.right * horizontal + camTransform.forward * vertical;
            HandleMovementServerRpc(direction, camTransform.forward);
        }

        [ServerRpc(RequireOwnership = false)]
        private void HandleMovementServerRpc(Vector3 direction, Vector3 forward)
        {
            if (!animator)
                return;

            animator.SetBool(AnimationConstants.Player.isMoving, false);

            if (direction.x != 0 || direction.y != 0)
            {
                animator.SetBool(AnimationConstants.Player.isMoving, true);
                Quaternion toRotation = Quaternion.LookRotation(forward - new Vector3(0, -0.2f, 0), new Vector3(0, 1, 0));
                skelleton.rotation = Quaternion.Slerp(skelleton.rotation, toRotation, rotationSpeed * Time.deltaTime);
            }

            hips.AddForce(direction * movementSpeed);
        }


        private void TryHitEnemy() {
            CancelInvoke(nameof(ResetHitCooldown));
            Invoke(nameof(ResetHitCooldown), hitAnimationTime);
            canHitEnemy = true;
        }

        private void ResetHitCooldown()
        {
            canHitEnemy = true;
        }

        private void HandleKick()
        {
            if (Input.GetKey(KeyCode.Mouse1) && animator && !atackCooldownOn)
            {
                animator.SetTrigger(AnimationConstants.Player.isKicking);
                animator.SetBool(AnimationConstants.Player.isLeftKick, isLeftKick);
                isLeftKick = !isLeftKick;
                StartCoroutine(StartAtackCooldown());
                TryHitEnemy();
            }
        }

        private void HandlePunch()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && animator && !atackCooldownOn)
            {
                animator.SetBool(AnimationConstants.Player.LeftHandUp, true);
                StartCoroutine(StartAtackCooldown());
                TryHitEnemy();
            }

            if (Input.GetKeyUp(KeyCode.Mouse0) && animator)
            {
                animator.SetBool(AnimationConstants.Player.LeftHandUp, false);
            }

            if (Input.GetKeyDown(KeyCode.Mouse1) && animator && !atackCooldownOn)
            {
                animator.SetBool(AnimationConstants.Player.RightHandUp, true);
                StartCoroutine(StartAtackCooldown());
                TryHitEnemy();
            }

            if (Input.GetKeyUp(KeyCode.Mouse1) && animator)
            {
                animator.SetBool(AnimationConstants.Player.RightHandUp, false);
            }
        }

        private IEnumerator StartAtackCooldown()
        {
            atackCooldownOn = true;
            yield return new WaitForSeconds(atackCooldown);
            atackCooldownOn = false;
        }

        private void InitializeMotion(Component dollTransform)
        {
            var dollBones = dollTransform.GetComponentsInChildren<Rigidbody>();
            var playerBones = GetComponentsInChildren<Rigidbody>();

            foreach (var dollBone in dollBones)
            {
                var playerBone = Array.Find(playerBones, bone => bone.name == dollBone.name);
                if (playerBone && playerBone.TryGetComponent<CopyMotion>(out var motion))
                    motion.TargetLimb = dollBone.transform;
            }
        }
    }
}