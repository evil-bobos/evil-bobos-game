﻿namespace EvilBobos.Gameplay.Player
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public interface IPlayerManager : ISubManager
    {
    }
    
    public class PlayerManager : MonoBehaviour, IPlayerManager
    {
        [SerializeField] private PlayerAnimatorController animatorController;
        [SerializeField] private float minYPosition = -30f;
        
        private IGameContext gameContext;
        
        public void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
        }

        public void PostInitialize()
        {
        }

        public void UpdateManager()
        {
            if (!gameContext.LocalNetworkManager.IsServer || gameContext.LocalNetworkManager.Players == null)
                return;

            var players = new List<PlayerController>(gameContext.LocalNetworkManager.Players);
            foreach (var player in players)
            {
                if (player.Transform.parent.position.y <= minYPosition)
                {
                    gameContext.LocalNetworkManager.OnPlayerLost(player.OwnerClientId);
                }
            
                if (player.health.currentHealth <= 0)
                {
                    gameContext.LocalNetworkManager.OnPlayerLost(player.OwnerClientId);
                }
            }
        }
    }
}