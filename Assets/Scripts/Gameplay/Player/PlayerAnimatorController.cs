﻿namespace EvilBobos.Gameplay.Player
{
    using Constants;
    using Entities;

    public class PlayerAnimatorController : EntityAnimatorController
    {
        public void TriggerJump() => SetTrigger(AnimationConstants.Player.Jump);
    }
}