﻿namespace EvilBobos.Gameplay.Network
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using Player;
    using Unity.Netcode;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public interface ILocalNetworkManager
    {
        List<PlayerController> Players { get; }
        bool IsServer { get; }
        
        void OnPlayerLost(ulong playerId);
        void AssignPowerUpToPlayer(Transform item, Transform parent);
        void SpawnNetworkPowerUp(Transform item);

        void DestroyNetworkPowerUp(Transform item);
    }
    
    public class LocalNetworkManager : NetworkBehaviour, ISubManager, ILocalNetworkManager
    {
        [SerializeField] private GameObject playerPrefab;
        [SerializeField] private GameObject playerPrefab1;
        [SerializeField] private Animator voodooAnimator;
        [SerializeField] private Transform voodooTransform;
 
        public static LocalNetworkManager Instance { get; private set; }
        public List<PlayerController> Players { get; private set; }
        public bool IsServer => base.IsServer;
        
        private IGameContext gameContext;

        private void Awake()
        {
            Instance = this;
        }

        public void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
        }

        public void PostInitialize()
        {
        }
        
        public void UpdateManager()
        {
            
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            if (IsServer)
            {
                NetworkManager.SceneManager.OnLoadEventCompleted += SceneManager_OnLoadEventCompleted;
            }
        }

        public void OnPlayerLost(ulong playerId)
        {
            NetworkManager.Singleton.DisconnectClient(playerId);
            Players.Remove(Players.Find(player => player.OwnerClientId == playerId));
            SceneManager.LoadSceneAsync(Scenes.MainMenu.ToString());
        }

        public void AssignPowerUpToPlayer(Transform item, Transform parent)
        {
            if (!IsServer)
                return;
            
            item.GetComponent<NetworkObject>().Spawn();
            item.SetParent(parent);
        }

        public void SpawnNetworkPowerUp(Transform item)
        {
            if (!IsServer)
                return;
            
            item.GetComponent<NetworkObject>().Spawn();
        }
        
        public void DestroyNetworkPowerUp(Transform item)
        {
            if (!IsServer)
                return;
            
            item.GetComponent<NetworkObject>().Despawn();
           
        }
        
        bool firstIteration = false;
        private void SceneManager_OnLoadEventCompleted(string scenename, LoadSceneMode loadscenemode, List<ulong> clientscompleted, List<ulong> clientstimedout)
        {
            Players = new List<PlayerController>();
            foreach (var clientId in NetworkManager.ConnectedClientsIds)
            {
                Debug.Log($"[LocalNetworkManager] OnClientConnected: {clientId}");
                
                var playerGameObject = Instantiate(firstIteration ? playerPrefab : playerPrefab1);
                firstIteration = true;
                playerGameObject.GetComponent<NetworkObject>().SpawnAsPlayerObject(clientId, true);
                
                var playerController = playerGameObject.GetComponent<PlayerController>();
                playerController.Initialize(voodooAnimator, voodooTransform);
                
                Players.Add(playerController);
            }
        }
    }
}