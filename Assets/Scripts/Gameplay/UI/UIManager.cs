﻿namespace EvilBobos.Gameplay.UI
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Views;

    public interface IUIManager : ISubManager
    {
        T GetView<T>() where T : UIViewBase;
    }
    
    public class UIManager : MonoBehaviour, IUIManager
    {
        private List<UIViewBase> views;
        
        public void Initialize(IGameContext gameContext)
        {
            views = GetComponentsInChildren<UIViewBase>().ToList();
            
            foreach (var view in views)
            {
                view.Initialize(gameContext);
            }
        }

        public void PostInitialize()
        {
            foreach (var view in views)
            {
                view.PostInitialize();
            }
        }

        public void UpdateManager()
        {
            foreach (var view in views)
            {
                view.UpdateView();
            }
        }

        public T GetView<T>() where T : UIViewBase
        {
            return views.OfType<T>().FirstOrDefault();
        }
    }
}