﻿namespace EvilBobos.Gameplay.UI.Views
{
    using System;
    using UnityEngine;

    public abstract class UIViewBase : MonoBehaviour
    {
        [SerializeField] private UIPanelBase panel;
        [SerializeField] private bool isDefaultView;
        
        public event Action<UIViewBase> OnViewOpened;
        public event Action<UIViewBase> OnViewClosed;
        
        public bool IsOpen { get; private set; }

        protected IGameContext gameContext;

        public virtual void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
            panel.Initialize(gameContext);

            if (isDefaultView)
                Open();
            else 
                Close();
        }

        public virtual void PostInitialize()
        {
            panel.PostInitialize();
        }

        public virtual void UpdateView()
        {
            if (IsOpen)
                panel.UpdatePanel();
        }
        
        public virtual void Open()
        {
            IsOpen = true;
            panel.Open();
            OnViewOpened?.Invoke(this);
        }
        
        public virtual void Close()
        {
            IsOpen = false;
            panel.Close();
            OnViewClosed?.Invoke(this);
        }
    }
}