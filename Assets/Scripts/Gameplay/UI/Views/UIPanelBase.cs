﻿namespace EvilBobos.Gameplay.UI.Views
{
    using UnityEngine;

    public abstract class UIPanelBase : MonoBehaviour
    {
        [SerializeField] protected CanvasGroup canvasGroup;

        protected IGameContext gameContext;
        
        public virtual void Initialize(IGameContext gameContext)
        {
            this.gameContext = gameContext;
        }
        
        public virtual void PostInitialize()
        {
        }
        
        public virtual void UpdatePanel()
        {
        }
        
        public virtual void Open()
        {
            canvasGroup.alpha = 1f;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

        public virtual void Close()
        {
            canvasGroup.alpha = 0f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }
    }
}