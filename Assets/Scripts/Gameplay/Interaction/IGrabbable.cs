﻿namespace EvilBobos.Gameplay.Interaction
{
    using UnityEngine;

    public interface IGrabbable
    {
        Transform Transform { get; }
        
        void Grab(Rigidbody grabbedBy);
        void Release();
    }
}