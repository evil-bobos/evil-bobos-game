﻿namespace EvilBobos.Gameplay.Interaction
{
    using Unity.Netcode;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody))]
    public class InteractiveObject : NetworkBehaviour, IGrabbable
    {
        private HingeJoint hingeJoint;

        public Transform Transform => transform;

        public void Grab(Rigidbody grabbedBy)
        {
            hingeJoint = gameObject.AddComponent<HingeJoint>();
            hingeJoint.connectedBody = grabbedBy;
        }

        public void Release()
        {
            hingeJoint.connectedBody = null;
            Destroy(hingeJoint);
        }
    }
}