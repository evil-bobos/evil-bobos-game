﻿namespace EvilBobos.Gameplay.MainMenu
{
    using Core;
    using Lobby;
    using UnityEngine;
    using UnityEngine.UI;

    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameConfig config;
        [SerializeField] private Loader loader;
        [SerializeField] private LobbyController lobbyController;
        [SerializeField] private MainMenuViewsController viewsController;
        [SerializeField] private Button exitButton;
        
        private void Start()
        {
            lobbyController.Initialize(config);
            viewsController.ShowView(MainMenuViewType.MainMenu);
            exitButton.onClick.AddListener(HandleExitButton);
        }

        private static void HandleExitButton()
        {
            Application.Quit();
            
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
    }
}