﻿namespace EvilBobos.Gameplay.MainMenu
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using DG.Tweening;
    using Extensions;
    using Views;

    public enum MainMenuViewType
    {
        MainMenu,
        LobbyCreation,
        LobbyManagement,
        JoinLobby
    }

    public class MainMenuViewsController : MonoBehaviour
    {
        [Serializable]
        private class MainMenuView
        {
            [SerializeField] private MainMenuViewType type;
            [SerializeField] private MenuView view;
            
            public MainMenuViewType Type => type;
            public MenuView View => view;
        }
        
        [SerializeField] private Image background;
        [SerializeField] private List<MainMenuView> views;
        
        private MenuView currentView;
        
        public void ShowView(MainMenuViewType type)
        {
            if (currentView)
                currentView.OnViewClosed();
            
            foreach (var view in views)
            {
                var isActive = view.Type == type;
                view.View.gameObject.SetActive(isActive);

                if (isActive)
                {
                    currentView = view.View;
                    currentView.OnViewOpened();
                    
                    background.DOCrossfadeImage(view.View.Background, 0.5f);
                }
            }
        }
    }
}