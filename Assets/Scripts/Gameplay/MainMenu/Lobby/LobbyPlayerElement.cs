﻿namespace EvilBobos.Gameplay.MainMenu.Lobby
{
    using TMPro;
    using Unity.Services.Lobbies.Models;
    using UnityEngine;

    public class LobbyPlayerElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI playerNameText;

        public string PlayerId => player.Id;
        
        private Player player;
        
        public void Initialize(Player player)
        {
            this.player = player;
            playerNameText.text = player.Id;
        }
    }
}