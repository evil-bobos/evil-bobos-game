﻿namespace EvilBobos.Gameplay.MainMenu.Lobby
{
    using System;
    using System.ComponentModel;
    using TMPro;
    using Unity.Services.Lobbies.Models;
    using UnityEngine;
    using UnityEngine.UI;

    public class LobbyElementController : MonoBehaviour
    {
        private enum LockState
        {
            Private,
            Public
        }
        
        [Serializable]
        private class LobbyState
        {
            [SerializeField] private LockState lockState;
            [SerializeField] private Sprite icon;
            
            public LockState LockState => lockState;
            public Sprite Icon => icon;
        }

        [SerializeField] private Button joinButton;
        [SerializeField] private TextMeshProUGUI lobbyNameText;
        [SerializeField] private TextMeshProUGUI playersCountText;
        [SerializeField] private Image stateImage;
        [SerializeField] private LobbyState[] lobbyStates;
        
        private const string PlayersCountFormat = "{0}/{1}";

        public void Initialize(Lobby lobby, Action joinAction)
        {
            lobbyNameText.text = lobby.Name;
            playersCountText.text = string.Format(PlayersCountFormat, lobby.Players.Count, lobby.MaxPlayers);
            stateImage.sprite = GetStateSprite(lobby);
            
            joinButton.onClick.RemoveAllListeners();
            joinButton.onClick.AddListener(joinAction.Invoke);
        }

        private Sprite GetStateSprite(Lobby lobby)
        {
            var state = lobby.HasPassword ? LockState.Private : LockState.Public;
            foreach (var lobbyState in lobbyStates)
            {
                if (lobbyState.LockState == state)
                    return lobbyState.Icon;
            }

            return null;
        }
    }
}