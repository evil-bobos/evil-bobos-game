﻿namespace EvilBobos.Gameplay.MainMenu.Lobby
{
    using System.Collections.Generic;
    using System.Linq;
    using Constants;
    using Core;
    using Utilities;
    using TMPro;
    using Unity.Netcode;
    using Unity.Services.Authentication;
    using Unity.Services.Core;
    using Unity.Services.Core.Environments;
    using Unity.Services.Lobbies;
    using Unity.Services.Lobbies.Models;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;
    using Utilities.Multiplayer;

    public class LobbyController : MonoBehaviour, ILobbyController
    {
        [SerializeField] private MainMenuViewsController viewsController;
        [SerializeField] private Button createLobbyButton;
        [SerializeField] private Button joinLobbyButton;

        [Header("Lobby Creation")] 
        [SerializeField] private TMP_InputField lobbyNameInput;
        [SerializeField] private Toggle privateLobbyToggle;
        [SerializeField] private Button submitButton;
        
        [Header("Joining Lobby")]
        [SerializeField] private TMP_InputField lobbyCodeInput;
        [SerializeField] private Button joinButton;
        
        [Header("Lobby Management")]
        [SerializeField] private TextMeshProUGUI lobbyNameText;
        [SerializeField] private TextMeshProUGUI lobbyCodeText;
        [SerializeField] private ObjectPool playerListPool;
        [SerializeField] private Button leaveLobbyButton;
        [SerializeField] private Button startGameButton;
        
        [Header("Lobbies List")]
        [SerializeField] private ObjectPool lobbyListPool;
        [SerializeField] private Button refreshLobbiesButton;
        
        private Dictionary<string, LobbyPlayerElement> lobbyPlayers;
        private LobbyEventCallbacks lobbyEventCallbacks;
        private GameConfig gameConfig;
        private Lobby currentLobby;
        
        private float heartBeatTimer;

        private const int MaxLobbyPlayers = 4;
        private const float HeartBeatInterval = 10f;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void Initialize(GameConfig gameConfig)
        {
            this.gameConfig = gameConfig;
            
            InitializeLobbyServices();
            
            lobbyPlayers = new Dictionary<string, LobbyPlayerElement>();
            
            createLobbyButton.onClick.AddListener(OnCreateLobbyButtonClicked);
            joinLobbyButton.onClick.AddListener(OnJoinLobbyButtonClicked);
            
            submitButton.onClick.AddListener(OnCreationSubmitButtonClicked);
            joinButton.onClick.AddListener(OnJoinByCodeLobbyButtonClicked);
            
            refreshLobbiesButton.onClick.AddListener(ShowLobbiesList);
            
            leaveLobbyButton.onClick.AddListener(LeaveLobby);
            startGameButton.onClick.AddListener(StartGame);
        }

        private void Update()
        {
            HandleHeartBeat();
        }

        private void OnApplicationQuit()
        {
            if (currentLobby != null)
                LeaveLobby();
        }

        private async void LeaveLobby()
        {
            try
            {
                await LobbyService.Instance.RemovePlayerAsync(currentLobby.Id, AuthenticationService.Instance.PlayerId);
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError($"[LobbyController]: {e.Message}");
            }
        }

        private async void StartGame()
        {
            if (currentLobby != null && currentLobby.IsHost())
            {
                await MainApplication.Initialize();
                NetworkManager.Singleton.SceneManager.LoadScene(Scenes.GameplayScene.ToString(), LoadSceneMode.Single);
            }
        }

        private async void HandleHeartBeat()
        {
            if (currentLobby == null || SceneManager.GetActiveScene().name != Scenes.MainMenu.ToString())
                return;
            
            heartBeatTimer += Time.deltaTime;

            if (heartBeatTimer < HeartBeatInterval)
                return;
            
            heartBeatTimer = 0f;
            await LobbyService.Instance.SendHeartbeatPingAsync(currentLobby.Id);
            Debug.Log("[LobbyController]: Sent heartbeat ping");
        }

        private async void InitializeLobbyServices()
        {
            if (UnityServices.State != ServicesInitializationState.Uninitialized)
                return;

            var options = new InitializationOptions();
            options.SetEnvironmentName(gameConfig.Multiplayer.Environment.ToString().ToLower());
            
            await UnityServices.InitializeAsync(options);
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }

        private void OnCreateLobbyButtonClicked()
        {
            viewsController.ShowView(MainMenuViewType.LobbyCreation);
        }

        private void OnJoinLobbyButtonClicked()
        {
            viewsController.ShowView(MainMenuViewType.JoinLobby);
            ShowLobbiesList();
        }

        private async void OnCreationSubmitButtonClicked()
        {
            submitButton.interactable = false;
            
            try
            {
                var relayCode = await RelayHelper.CreateRelay(MaxLobbyPlayers);
                
                currentLobby = await LobbyService.Instance.CreateLobbyAsync(lobbyNameInput.text, MaxLobbyPlayers, new CreateLobbyOptions
                {
                    IsPrivate = privateLobbyToggle.isOn,
                    Data = new Dictionary<string, DataObject>
                    {
                        { LobbyOptions.StartGameCode, new DataObject(DataObject.VisibilityOptions.Member, relayCode) }
                    }
                });
                
                Debug.Log($"[LobbyController]: Created lobby {currentLobby.Name} [{currentLobby.Id}]");

                PlayerPrefs.SetString("LobbyId", currentLobby.Id);
                
                SubscribeToLobbyEvents();
                ShowLobbyInformation();
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError($"[LobbyController]: {e.Message}");
            }
            
            submitButton.interactable = true;
        }
        
        private async void OnJoinByCodeLobbyButtonClicked()
        {
            joinButton.interactable = false;
            
            try
            {
                var lobbyCode = lobbyCodeInput.text;
                currentLobby = await LobbyService.Instance.JoinLobbyByCodeAsync(lobbyCode);
                
                Debug.Log($"[LobbyController]: Joined lobby {currentLobby.Name} [{currentLobby.Id}]");
                
                PlayerPrefs.SetString("LobbyId", currentLobby.Id);
                
                RelayHelper.JoinRelay(currentLobby.Data[LobbyOptions.StartGameCode].Value);
                
                SubscribeToLobbyEvents();
                ShowLobbyInformation();
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError($"[LobbyController]: {e.Message}");
            }
            
            joinButton.interactable = true;
        }

        private async void ShowLobbyInformation()
        {
            viewsController.ShowView(MainMenuViewType.LobbyManagement);
            
            currentLobby = await LobbyService.Instance.GetLobbyAsync(currentLobby.Id);
            
            lobbyNameText.text = $"Lobby Name: {currentLobby.Name}";
            lobbyCodeText.text = $"Lobby Code: {currentLobby.LobbyCode}";

            foreach (var player in currentLobby.Players)
            {
                if (lobbyPlayers.ContainsKey(player.Id))
                    continue;
                
                var playerItem = playerListPool.Borrow().GetComponent<LobbyPlayerElement>();
                playerItem.Initialize(player);
                
                lobbyPlayers.Add(player.Id, playerItem);
            }
            
            leaveLobbyButton.gameObject.SetActive(!currentLobby.IsHost());
            startGameButton.gameObject.SetActive(currentLobby.IsHost());
        }
        
        private async void SubscribeToLobbyEvents()
        {
            lobbyEventCallbacks = new LobbyEventCallbacks();
            lobbyEventCallbacks.PlayerJoined += LobbyEventCallbacks_PlayerJoined;
            lobbyEventCallbacks.PlayerLeft += LobbyEventCallbacks_PlayerLeft;
            lobbyEventCallbacks.KickedFromLobby += LobbyEventCallbacks_KickedFromLobby;
            lobbyEventCallbacks.LobbyDeleted += LobbyEventCallbacks_LobbyDeleted;
            
            try {
                await Lobbies.Instance.SubscribeToLobbyEventsAsync(currentLobby.Id, lobbyEventCallbacks);
                Debug.Log("[LobbyController]: Subscribed to lobby events");
            }
            catch (LobbyServiceException ex)
            {
                switch (ex.Reason) {
                    case LobbyExceptionReason.AlreadySubscribedToLobby: 
                        Debug.LogWarning($"[LobbyController]: Already subscribed to lobby [{currentLobby.Id}]. Exception Message: {ex.Message}"); 
                        break;
                    case LobbyExceptionReason.SubscriptionToLobbyLostWhileBusy: 
                        Debug.LogError($"[LobbyController]: Subscription to lobby events was lost while it was busy trying to subscribe. Exception Message: {ex.Message}"); 
                        throw;
                    case LobbyExceptionReason.LobbyEventServiceConnectionError: 
                        Debug.LogError($"[LobbyController]: Failed to connect to lobby events. Exception Message: {ex.Message}"); 
                        throw;
                    default: throw;
                }
            }
        }

        private void UnsubscribeFromLobbyEvents()
        {
            lobbyEventCallbacks = null;
        }

        private async void ShowLobbiesList()
        {
            try
            {
                var queryOptions = new QueryLobbiesOptions
                {
                    Count = 25,
                    Filters = new List<QueryFilter>
                    {
                        new(QueryFilter.FieldOptions.AvailableSlots, "0", QueryFilter.OpOptions.GT)
                    },
                    Order = new List<QueryOrder>
                    {
                        new(false, QueryOrder.FieldOptions.Created)
                    }
                };
                
                var lobbies = await LobbyService.Instance.QueryLobbiesAsync(queryOptions);
                
                lobbyListPool.ReleaseAll();
                foreach (var lobby in lobbies.Results)
                {
                    var lobbyItem = lobbyListPool.Borrow().GetComponent<LobbyElementController>();
                    lobbyItem.Initialize(lobby, () => JoinLobby(lobby.Id));
                }
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError($"[LobbyController]: {e.Message}");
            }
        }
        
        private async void JoinLobby(string lobbyId)
        {
            currentLobby = await LobbyService.Instance.JoinLobbyByIdAsync(lobbyId);
            
            Debug.Log($"[LobbyController]: Joined lobby {currentLobby.Name} [{currentLobby.Id}]");
                
            PlayerPrefs.SetString("LobbyId", currentLobby.Id);
            
            RelayHelper.JoinRelay(currentLobby.Data[LobbyOptions.StartGameCode].Value);
            
            SubscribeToLobbyEvents();
            ShowLobbyInformation();
        }

        private void LobbyEventCallbacks_PlayerJoined(List<LobbyPlayerJoined> players)
        {
            foreach (var player in players)
            {
                Debug.Log($"[LobbyController]: Player {player.Player.Id} joined lobby {currentLobby.Name} [{currentLobby.Id}]");
                
                var playerItem = playerListPool.Borrow().GetComponent<LobbyPlayerElement>();
                playerItem.Initialize(player.Player);
                
                lobbyPlayers.Add(player.Player.Id, playerItem);
            }
        }

        private void LobbyEventCallbacks_PlayerLeft(List<int> playersIndices)
        {
            foreach (var playerIndex in playersIndices)
            {
                Debug.Log($"[LobbyController]: Player [{playerIndex}] left lobby {currentLobby.Name} [{currentLobby.Id}]");
                
                var playerItem = lobbyPlayers.ElementAt(playerIndex);
                
                playerListPool.Release(playerItem.Value.gameObject);
                lobbyPlayers.Remove(playerItem.Key);
            }
        }

        private void LobbyEventCallbacks_KickedFromLobby()
        {
            Debug.Log($"[LobbyController]: Kicked from lobby {currentLobby.Name} [{currentLobby.Id}]");
            
            UnsubscribeFromLobbyEvents();
            
            currentLobby = null;
            PlayerPrefs.DeleteKey("LobbyId");
            
            viewsController.ShowView(MainMenuViewType.MainMenu);
        }

        private void LobbyEventCallbacks_LobbyDeleted()
        {
            Debug.Log($"[LobbyController]: Lobby {currentLobby.Name} [{currentLobby.Id}] was deleted");
            
            UnsubscribeFromLobbyEvents();
            
            currentLobby = null;
            PlayerPrefs.DeleteKey("LobbyId");
            
            viewsController.ShowView(MainMenuViewType.MainMenu);
        }
    }
}