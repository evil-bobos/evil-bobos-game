﻿namespace EvilBobos.Gameplay.MainMenu.Views
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;

    public class MainMenuView : MenuView
    {
        [SerializeField] private Image connectionStatus;
        [SerializeField] private GameObject crosshair;
        [SerializeField] private float pingInterval = 2f;
        [SerializeField] private float pingTimeout = 5f;
        
        private float lastPingTime;
        private bool isPingInProgress;
        
        private const string TestAddress = "8.8.8.8";

        public override void OnViewOpened()
        {
            base.OnViewOpened();

            StartCoroutine(HandleTestPing());
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed();
            
            StopAllCoroutines();
        }

        private void Update()
        {
            if (Time.time - lastPingTime >= pingInterval && !isPingInProgress)
            {
                StartCoroutine(HandleTestPing());
            }
        }

        private IEnumerator HandleTestPing()
        {
            var connectionTest = new Ping(TestAddress);
            var startTime = Time.time;
            isPingInProgress = true;
                
            while (!connectionTest.isDone)
            {
                if (Time.time - startTime >= pingTimeout)
                {
                    Debug.Log("[MainMenuView]: Connection test timed out");
                    ShowResult(false);
                    yield break;
                }
                
                yield return null;
            }
            
            Debug.Log($"[MainMenuView]: Connection test resulted with {connectionTest.time} ms");
            ShowResult(true);
        }
        
        private void ShowResult(bool isSuccess)
        {
            crosshair.SetActive(!isSuccess);
            lastPingTime = Time.time;
            isPingInProgress = false;
        }

        private void OnApplicationQuit()
        {
            StopAllCoroutines();
        }
    }
}