﻿namespace EvilBobos.Gameplay.MainMenu.Views
{
    using UnityEngine;

    public class MenuView : MonoBehaviour
    {
        [SerializeField] private Sprite background;

        public Sprite Background => background;

        public virtual void OnViewOpened()
        {
        }
        
        public virtual void OnViewClosed()
        {
        }
    }
}