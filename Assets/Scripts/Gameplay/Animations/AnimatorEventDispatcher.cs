﻿namespace EvilBobos.Gameplay.Animations
{
    using System;
    using UnityEngine;

    public class AnimatorEventDispatcher : MonoBehaviour
    {
        public event Action<string> OnAnimationTrigger;
        
        public void AnimationTrigger(string triggerName)
        {
            OnAnimationTrigger?.Invoke(triggerName);
        }
    }
}