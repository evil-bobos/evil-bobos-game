﻿namespace EvilBobos.Gameplay.Entities
{
    using UnityEngine;

    public class EntityAnimatorController : MonoBehaviour
    {
        [SerializeField] protected Animator animator;

        private int MainLayerState => animator.GetCurrentAnimatorStateInfo(0).shortNameHash;

        protected bool IsInBaseAnimation()
        {
            return MainLayerState == BodyPositionHelper.IdleAnimation
                   || MainLayerState == BodyPositionHelper.CrouchAnimation;
        }
        
        public void SetFloat(int id, float value) => animator.SetFloat(id, value);
        public void SetBool(int id, bool value) => animator.SetBool(id, value);
        public void SetTrigger(int id) => animator.SetTrigger(id);
        public void SetInteger(int id, int value) => animator.SetInteger(id, value);
    }
}