using System.Collections;
using System.Collections.Generic;
using EvilBobos.Gameplay.Player;
using UnityEngine;

namespace EvilBobos
{
    public class SpeedBooster : PowerUp
    {
        [SerializeField] private float movementSpeedTime = 5f;
        [SerializeField] private float movementSpeedBoost = 2f;
        public override PowerUpType type => PowerUpType.Running;

        public override void UsePowerUp()
        {
            playerController.MovementSpeed *= movementSpeedBoost;
            Invoke(nameof(ClearPowerUp), movementSpeedTime);
        }

        private void ClearPowerUp()
        {
            playerController.MovementSpeed /= movementSpeedBoost;
        }
    }
}
