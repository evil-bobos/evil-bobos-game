using UnityEngine;

namespace EvilBobos
{
    using EvilBobos.Gameplay.Player;
    using Gameplay.Network;

    public abstract class PowerUp : MonoBehaviour
    {
        [Header("Boost")]
        [SerializeField] public Transform RefItem;
        [SerializeField] private float RespawnTime = 10f;
        public abstract PowerUpType type { get; }
        [HideInInspector]
        public enum PowerUpType
        {
            Null,
            Running,
            Jumping
        }
        private Transform item;
        [HideInInspector] public PlayerController playerController;

        [Header("Icon")]
        [SerializeField] private float rotationSpeed = 50f;
        [SerializeField] private float verticalSpeed = 2f;
        [SerializeField] private float verticalAmplitude = 1f;
        [HideInInspector] public bool spawned = false;
        private float originalY;

        public void Start()
        {
            originalY = transform.position.y;
            if (!spawned) Invoke(nameof(SpawnItem), RespawnTime);
        }

        public void Update()
        {
            if (item != null)
            {
                item.transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
                float verticalMovement = Mathf.Sin(Time.time * verticalSpeed) * verticalAmplitude;
                item.transform.position = new Vector3(transform.position.x, originalY + verticalMovement, transform.position.z);
            }
        }

        public PowerUpType GetPowerUpType() {
            return type;
        }

        public void SpawnItem()
        {
            spawned = true;
            item = Instantiate(RefItem, transform.position, Quaternion.identity).transform;
            LocalNetworkManager.Instance.AssignPowerUpToPlayer(item, transform);
        }

        public void PickItem()
        {
            if (spawned)
            {
                Destroy(item.gameObject);
                spawned = false;
                Invoke(nameof(SpawnItem), RespawnTime);
            }
        }

        public abstract void UsePowerUp();
    }
}
