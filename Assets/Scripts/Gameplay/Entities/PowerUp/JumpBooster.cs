using System.Collections;
using System.Collections.Generic;
using EvilBobos.Gameplay.Player;
using UnityEngine;

namespace EvilBobos
{
    public class JumpBooster : PowerUp
    {
        [SerializeField] private float jumpTime = 5f;
        [SerializeField] private float jumpBoost = 2f;
         public override PowerUpType type => PowerUpType.Jumping;

        public override void UsePowerUp()
        {
            playerController.JumpForce *= jumpBoost;
            Invoke(nameof(ClearPowerUp), jumpTime);
        }

        private void ClearPowerUp()
        {
            playerController.JumpForce /= jumpBoost;
        }
    }
}
