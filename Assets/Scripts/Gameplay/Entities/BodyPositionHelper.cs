﻿namespace EvilBobos.Gameplay.Entities
{
    using UnityEngine;

    public static class BodyPositionHelper
    {
        public static readonly int IdleAnimation = Animator.StringToHash("Idle");
        public static readonly int CrouchAnimation = Animator.StringToHash("Crouch");
    }
}